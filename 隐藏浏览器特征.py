import time
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options

chrome_options = Options()
chrome_options.add_argument("--headless")
# 添加请求头
chrome_options.add_argument('user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36')

brower = Chrome('chromedriver', options=chrome_options)

with open('stealth.min.js') as f:
    js = f.read()

# 在打印具体的网页前，执行隐藏浏览器特征的JavaScript
brower.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
  "source": js
})
brower.get('https://bot.sannysoft.com/')
time.sleep(5)
brower.save_screenshot('walkaround.png')

# 保存网站完整的HTML代码，方便查阅
source = brower.page_source
with open('result.html', 'w') as f:
    f.write(source)
